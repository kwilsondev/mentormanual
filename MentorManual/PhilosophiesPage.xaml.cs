﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace MentorManual
{
    public partial class PhilosophiesPage : ContentPage
    {
        public PhilosophiesPage()
        {
            InitializeComponent();

            if(Device.RuntimePlatform == Device.iOS){
                Title = "";
                ToolbarItems.Add(new ToolbarItem() { Icon = "iosShareIcon.png", Command = new Command(() => SharePressed() )});
            }
            else
            {
                ToolbarItems.Add(new ToolbarItem() { Icon = "androidShareIcon.png", Command = new Command(() => SharePressed()) });

            }

        }

        public void setBodyText(String textIn, bool isCentered){

            philosophyBody.Text = textIn;

            if(isCentered){
                
                philosophyBody.HorizontalTextAlignment = TextAlignment.Center;

            } else {
                
                philosophyBody.HorizontalTextAlignment = TextAlignment.Start;

            }

        }

        private async void SharePressed()
        {
            await Share.RequestAsync(new ShareTextRequest {
                Text = philosophyBody.Text + "\n\n-Shared from the Mentor Manual Mobile App",
                Title = this.Title
              });
        }
    }
}
