﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Xamarin.Forms;

namespace MentorManual
{
    public partial class BedohaveEntryPage : ContentPage
    {

	    

        public BedohaveEntryPage()
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.iOS)
            {
                Title = "";
            }

            if (Application.Current.Properties.ContainsKey("bedohaveString")) {
                bedohaveEditor.Text = Application.Current.Properties["bedohaveString"].ToString();
            }


        }

        async void EntryTextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            Application.Current.Properties["bedohaveString"] = e.NewTextValue;
            await Application.Current.SavePropertiesAsync();
        }


    }
}
