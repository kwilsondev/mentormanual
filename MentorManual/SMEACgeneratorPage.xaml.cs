﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;

namespace MentorManual
{
    public partial class SMEACgeneratorPage : ContentPage
    {

        int obstacleCount = 0;
        int supportCount = 0;

        List<string> obstacles = new List<string>();
        List<string> supports = new List<string>();
        List<string> tasks = new List<string>();


        Command obstacleCommand;
        Command supportCommand;
        Command taskCommand;


        public SMEACgeneratorPage()
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.iOS)
            {
                Title = "";
            }

            //create delete image commands
            obstacleCommand = new Command<string>(DeleteObstacleTapped);
            supportCommand = new Command<string>(DeleteSupportTapped);
            taskCommand = new Command<string>(DeleteTaskTapped);

            ToolbarItem help = new ToolbarItem("Help", null, SmeacHelpTapped);
            ToolbarItems.Add(help);

        }

        void SmeacHelpTapped()
        {
            Debug.WriteLine("Help Tapped");

            PhilosophiesPage newPage = new PhilosophiesPage();
            newPage.setBodyText(PhilosophyResources.smeac, false);

            //if (Device.RuntimePlatform != Device.iOS)
            //{
                newPage.Title = "SMEAC";
            //}

            //NavigationPage smeacPage = new NavigationPage(newPage)
            //{
            //    BarTextColor = Color.White,
            //    BarBackgroundColor = Color.FromHex("#0d4816")
            //};

            Navigation.PushAsync(newPage);


        }


        void DeleteObstacleTapped(String obstacleName)
        {
            Debug.WriteLine(obstacleName);

            int indexToDelete = -1;

            foreach (StackLayout stack in obstacleList.Children){
                
                if (stack.AutomationId == obstacleName){
                    
                    indexToDelete = obstacleList.Children.IndexOf(stack);

                }

            }

            if (indexToDelete != -1){
                
                obstacleList.Children.RemoveAt(indexToDelete);

            }
        }

        void DeleteSupportTapped(String supportName)
        {
            Debug.WriteLine(supportName);

            int indexToDelete = -1;

            foreach (StackLayout stack in supportList.Children)
            {

                if (stack.AutomationId == supportName)
                {

                    indexToDelete = supportList.Children.IndexOf(stack);

                }

            }

            if (indexToDelete != -1)
            {

                supportList.Children.RemoveAt(indexToDelete);

            }

        }

        void DeleteTaskTapped(String taskName)
        {
            Debug.WriteLine(taskName);

            int indexToDelete = -1;

            foreach (StackLayout stack in taskList.Children)
            {

                if (stack.AutomationId == taskName)
                {

                    indexToDelete = taskList.Children.IndexOf(stack);

                }

            }

            if (indexToDelete != -1)
            {

                taskList.Children.RemoveAt(indexToDelete);

            }

        }

        void ObstacleAddTapped(Object sender, System.EventArgs e){

            Debug.WriteLine("Obstacle Added");

            if (obstacleEntry.Text == "") return;
               
            obstacleCount++;

            Label obstacle = new Label
            {
                Text = obstacleEntry.Text,
                FontSize = 20,
                TextColor = Color.White,
                Margin = new Thickness(10,10,0,0)
                                    
            };

            StackLayout stack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                AutomationId = obstacleEntry.Text
            };

            Image minusImage = new Image
            {
                Source = "minusIcon.png",
                WidthRequest = 25,
                Margin = new Thickness(10,5,0,0)

            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = obstacleCommand,
                CommandParameter = obstacleEntry.Text
            };

            minusImage.GestureRecognizers.Add(tapGestureRecognizer);

            stack.Children.Add(minusImage);
            stack.Children.Add(obstacle);

            obstacleList.Children.Add(stack);

            obstacleEntry.Text = "";
            obstacleEntry.Focus();
                        
        }

        void SupportAddTapped(Object sender, System.EventArgs e){
            
            Debug.WriteLine("Support Added");

            if (supportEntry.Text == "") return;

            supportCount++;

            Label support = new Label
            {
                Text = supportEntry.Text,
                FontSize = 20,
                TextColor = Color.White,
                Margin = new Thickness(10, 10, 0, 0)

            };

            StackLayout stack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                AutomationId = supportEntry.Text
            };

            Image minusImage = new Image
            {
                Source = "minusIcon.png",
                WidthRequest = 25,
                Margin = new Thickness(10, 5, 0, 0)

            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = supportCommand,
                CommandParameter = supportEntry.Text
            };

            minusImage.GestureRecognizers.Add(tapGestureRecognizer);

            stack.Children.Add(minusImage);
            stack.Children.Add(support);

            supportList.Children.Add(stack);

            supportEntry.Text = "";
            supportEntry.Focus();

        }

        void TaskAddTapped(Object sender, System.EventArgs e)
        {

            Debug.WriteLine("Task Added");

            if (taskEntry.Text == "") return;

            Label task = new Label
            {
                Text = taskEntry.Text,
                FontSize = 20,
                TextColor = Color.White,
                Margin = new Thickness(10, 10, 0, 0)

            };

            StackLayout stack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                AutomationId = taskEntry.Text
            };

            Image minusImage = new Image
            {
                Source = "minusIcon.png",
                WidthRequest = 25,
                Margin = new Thickness(10, 5, 0, 0)

            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = taskCommand,
                CommandParameter = taskEntry.Text
            };

            minusImage.GestureRecognizers.Add(tapGestureRecognizer);

            stack.Children.Add(minusImage);
            stack.Children.Add(task);

            taskList.Children.Add(stack);

            taskEntry.Text = "";
            taskEntry.Focus();

        }



        void GenerateSMEAC(object sender, System.EventArgs e)
        {
            StringBuilder smeac = new StringBuilder();

            //populate obstacle list
            foreach(StackLayout obstacle in obstacleList.Children){

                Label obstacleLabel = (Label)obstacle.Children[1];

                //Debug.WriteLine(obstacleLabel.Text);
                obstacles.Add(obstacleLabel.Text);

            }

            //populate support list
            foreach (StackLayout support in supportList.Children)
            {

                Label supportLabel = (Label)support.Children[1];

                //Debug.WriteLine(supportLabel.Text);
                supports.Add(supportLabel.Text);
            }

            //populate task list
            foreach (StackLayout task in taskList.Children)
            {
                Label taskLabel = (Label)task.Children[1];

                //Debug.WriteLine(taskLabel.Text);
                tasks.Add(taskLabel.Text);
            }

            //Situation
            smeac.AppendLine("========Situation============");
            smeac.Append("The obstacles that I will face are: ");

            //obstacles
            foreach(string obstacle in obstacles){
                smeac.Append(obstacle + ", ");
            }

            smeac.Remove(smeac.Length - 2, 2);
            smeac.AppendLine();
            smeac.AppendLine();
            smeac.Append("I will draw support from: ");

            //support
            foreach (string support in supports)
            {
                smeac.Append(support + ", ");
            }

            smeac.Remove(smeac.Length - 2, 2);
            smeac.AppendLine();

            //Mission
            smeac.AppendLine();
            smeac.AppendLine("========Mission=============");
            smeac.AppendLine(missionEditor.Text);
            smeac.AppendLine();

            //Execution
            smeac.AppendLine("========Execution===========");
            smeac.AppendLine("Individual tasks are as follows:");

            foreach (string task in tasks)
            {
                smeac.AppendLine("-> " + task);
            }
            smeac.AppendLine();

            //Administration
            smeac.AppendLine("========Administration========");
            smeac.AppendLine("Food: " + foodEntry.Text);
            smeac.AppendLine("First Aid: " + firstAidEntry.Text);
            smeac.AppendLine("Equipment: " + equipmentEntry.Text);
            smeac.AppendLine("Transportation: " + transportEntry.Text);
            smeac.AppendLine();
            smeac.AppendLine("Time: " + timeEntry.Text);
            smeac.AppendLine("People: " + peopleEntry.Text);
            smeac.AppendLine("Money: " + moneyEntry.Text);
            smeac.AppendLine("Facilities: " + facilitiesEntry.Text);
            smeac.AppendLine();

            //Communication
            smeac.AppendLine("========Communication========");
            smeac.AppendLine(communicationEditor.Text);

            SMEACViewerPage viewerPage = new SMEACViewerPage();
            viewerPage.SetBodyText(smeac.ToString());

            Navigation.PushAsync(viewerPage);

            Debug.WriteLine(smeac);



        }
    }
}
