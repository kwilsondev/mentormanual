﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace MentorManual
{
    public partial class BedohaveListPage : ContentPage
    {
        string itemType;
        int itemCount;

        List<string> items = new List<string>();


        Command itemCommand;


        public BedohaveListPage(){
            InitializeComponent();

        }

        public BedohaveListPage(string type, Image icon)
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.iOS)
            {
                Title = "";
            }

            titleLabel.Text = type;
            itemType = type;

            //create delete image command
            itemCommand = new Command<string>(DeleteItemTapped);

            //load old items depending on page type
            switch(itemType){

                case "BE":

                    if(Application.Current.Properties.ContainsKey("beItems")){
                        items = JsonConvert.DeserializeObject<List<string>>(Application.Current.Properties["beItems"].ToString());
                    } else {
                        items = new List<string>();
                    }


                    break;

                case "DO":

                    if (Application.Current.Properties.ContainsKey("doItems"))
                    {
                        items = JsonConvert.DeserializeObject<List<string>>(Application.Current.Properties["doItems"].ToString());
                    }
                    else
                    {
                        items = new List<string>();
                    }

                    break;

                case "HAVE":

                    if (Application.Current.Properties.ContainsKey("haveItems"))
                    {
                        items = JsonConvert.DeserializeObject<List<string>>(Application.Current.Properties["haveItems"].ToString());
                    }
                    else
                    {
                        items = new List<string>();
                    }

                    break;

            }

            itemCount = items.Count;

            //add previously added items to screen
            foreach (String item in items)
            {
                AddItem(item);
            }

        }

        async void AddItemTapped(object sender, System.EventArgs e)
        {

            //check for empty tap
            if (itemEntry.Text == "") return;

            //update item list and serialize the new list
            itemCount++;
            items.Add(itemEntry.Text);
            var itemsJson = JsonConvert.SerializeObject(items);

            switch (itemType)
            {

                case "BE":

                    Application.Current.Properties["beItems"] = itemsJson;
                    await Application.Current.SavePropertiesAsync();
                    AddItem();
                    break;

                case "DO":

                    Application.Current.Properties["doItems"] = itemsJson;
                    await Application.Current.SavePropertiesAsync();
                    AddItem();
                    break;

                case "HAVE":

                    Application.Current.Properties["haveItems"] = itemsJson;
                    await Application.Current.SavePropertiesAsync();
                    AddItem();
                    break;

            }

        }

        async void DeleteItemTapped(String itemName)
        {
            Debug.WriteLine(itemName);

            int indexToDelete = -1;

            foreach (StackLayout stack in itemList.Children)
            {

                if (stack.AutomationId == itemName)
                {

                    indexToDelete = itemList.Children.IndexOf(stack);

                }

            }

            if (indexToDelete != -1)
            {

                itemList.Children.RemoveAt(indexToDelete);
                items.RemoveAt(indexToDelete);

            }


            var itemsJson = JsonConvert.SerializeObject(items);

            switch (itemType)
            {

                case "BE":

                    Application.Current.Properties["beItems"] = itemsJson;
                    await Application.Current.SavePropertiesAsync();
                    break;

                case "DO":

                    Application.Current.Properties["doItems"] = itemsJson;
                    await Application.Current.SavePropertiesAsync();
                    break;

                case "HAVE":

                    Application.Current.Properties["haveItems"] = itemsJson;
                    await Application.Current.SavePropertiesAsync();
                    break;

            }


        }

        void AddItem(){

            Label item = new Label
            {
                Text = itemEntry.Text,
                FontSize = 20,
                TextColor = Color.White,
                Margin = new Thickness(10, 10, 0, 0)

            };

            StackLayout stack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                AutomationId = itemEntry.Text
            };

            Image minusImage = new Image
            {
                Source = "minusIcon.png",
                WidthRequest = 25,
                Margin = new Thickness(10, 5, 0, 0)

            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = itemCommand,
                CommandParameter = itemEntry.Text
            };

            minusImage.GestureRecognizers.Add(tapGestureRecognizer);

            stack.Children.Add(minusImage);
            stack.Children.Add(item);

            itemList.Children.Add(stack);

            itemEntry.Text = "";
            itemEntry.Focus();

        }

        void AddItem(string itemText)
        {

            Label item = new Label
            {
                Text = itemText,
                FontSize = 20,
                TextColor = Color.White,
                Margin = new Thickness(10, 10, 0, 0)

            };

            StackLayout stack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                AutomationId = itemText
            };

            Image minusImage = new Image
            {
                Source = "minusIcon.png",
                WidthRequest = 25,
                Margin = new Thickness(10, 5, 0, 0)

            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = itemCommand,
                CommandParameter = itemText
            };

            minusImage.GestureRecognizers.Add(tapGestureRecognizer);

            stack.Children.Add(minusImage);
            stack.Children.Add(item);

            itemList.Children.Add(stack);

            itemEntry.Text = "";
            itemEntry.Focus();



        }

    }
}
