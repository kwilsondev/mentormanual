﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Xamarin.Forms;

namespace MentorManual
{
    public partial class MindGamesPage : ContentPage
    {
        public MindGamesPage()
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.iOS)
            {
                Title = "";
            }

        }

        void greenGlassEntryChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (e.NewTextValue == ""){
                greenGlassCheckButton.IsEnabled = false;
                greenGlassResult.Text = "...";
            } else {
                greenGlassCheckButton.IsEnabled = true;
            }
        }

        void greenGlassDoorsCheck(object sender, System.EventArgs e)
        {
            String input = greenGlassEntry.Text;
            input = input.ToLower();
            bool success = false;

            if(input.Length == 2 && input[0].Equals(input[1])){
                success = true;
            }

            foreach (Char currentLetter in input){

                if(input.IndexOf(currentLetter) != input.Length - 1){

                    if (currentLetter.Equals(input[input.IndexOf(currentLetter) + 1]))
                    {
                        success = true;
                        break;
                    }

                }

            }

            if (success) {
                
                greenGlassResult.Text = "That can go through!";

            } else {

                greenGlassResult.Text = "That cannot go through!";

            }
        }

    }
}
