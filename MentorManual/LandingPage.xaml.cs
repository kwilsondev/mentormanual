﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MentorManual
{
    public partial class LandingPage : ContentPage
    {
        public LandingPage()
        {
            InitializeComponent();
        }


        void URLTapped(object sender, System.EventArgs e)
        {
            Uri odoWebsite = new Uri("http://outdoorodyssey.org");

            Device.OpenUri(odoWebsite);
        }

        void phoneNumberTapped(object sender, System.EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Phone clicked");

            Device.OpenUri(new Uri(String.Format("tel:+18146296516")));

        }

    }
}
