﻿using Xamarin.Forms;

namespace MentorManual
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            NavigationPage navPage = new NavigationPage(new LandingPage());
            navPage.BackgroundColor = Color.FromHex("#726c5d");
            navPage.BarBackgroundColor = Color.FromHex("#0d4816");
            navPage.BarTextColor = Color.White;

            MainPage = new MasterDetailPage()
            {
                Master = new MasterPage()
                {
                    Title = "Menu",
                    BackgroundColor = Color.FromHex("#0d4816")
                },

            

                Detail = navPage
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
