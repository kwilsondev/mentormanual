﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Xamarin.Forms;

namespace MentorManual
{
    public partial class MasterPage : ContentPage
    {

        MasterDetailPage mdp;
        List<Label> philosophyList;
        List<Label> sTimeList;


        public MasterPage()
        {
            InitializeComponent();

            //populate philosophyList with all philosophy labels
            philosophyList = new List<Label>
            {
                thirtyOneLabel,
                icebergLabel,
                tlsLabel,
                ebaLabel,
                wizardLabel,
                potatoLabel,
                microwaveLabel,
                fiveLabel,
                bedohaveLabel,
                darLabel,
                smeacLabel,
                mepsLabel,
                traitsLabel,
                principlesLabel
            };

            //populate sTimeList with all S Time labels
            sTimeList = new List<Label>
            {
                aCreedLabel,
                aWishLabel,
                anywayLabel,
                attitudeLabel,
                birdLabel,
                conversationsLabel,
                desiderataLabel,
                dontWeLabel,
                faithTrustLabel,
                footprintsLabel,
                godsPromiseLabel,
                howToMakeLabel,
                ifLabel,
                ifEverLabel,
                ifIWereLabel,
                justToLabel,
                lincolnsLabel,
                littleEyesBoyLabel,
                littleEyesGirlLabel,
                liveYourDashLabel,
                mayonnaiseLabel,
                myPledgeLabel,
                paidInFullLabel,
                quotesLabel,
                rememberLabel,
                spreadingYourLightLabel,
                strivingLabel,
                successEmersonLabel,
                successUnknownLabel,
                bridgeBuilderLabel,
                carrotEggLabel,
                emperorsSeedLabel,
                generalsPoemLabel,
                manInTheArenaLabel,
                manInTheGlassLabel,
                optimistsCreedLabel,
                paradoxLabel,
                prayerOfASportsmanLabel,
                roadNotTakenLabel,
                sheepdogsPoemLabel,
                starfishLabel,
                thisIsTheBeginningLabel,
                toRiskLabel,
                universalPrayerLabel,
                vigilLabel,
                weShallLabel
            };

            if (Device.RuntimePlatform == Device.iOS)
            {
                androidSearchGrid.IsVisible = false;
                androidSearchFrame.IsVisible = false;
                iosSearchBar.IsVisible = true;
            }
            else
            {
                androidSearchGrid.IsVisible = true;
                androidSearchFrame.IsVisible = true;
                iosSearchBar.IsVisible = false;
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            mdp = Application.Current.MainPage as MasterDetailPage;

        }

        public void searchTextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            String input = e.NewTextValue;

            if (input != "")
            {

                foreach (Label philosophy in philosophyList)
                {
                    philosophy.IsVisible = philosophy.Text.ToLower().Contains(input.ToLower());

                    if (input.Equals(""))
                    {
                        philosophy.IsVisible = false;
                        philosophyLabel.TextColor = Color.White;
                        sTimeLabel.TextColor = Color.White;
                    }

                }

                foreach (Label sTime in sTimeList)
                {
                    sTime.IsVisible = sTime.Text.ToLower().Contains(input.ToLower());

                    sTime.IsVisible &= !input.Equals("");

                }

            }
            else
            {

                philosophyLabel.TextColor = Color.White;

                foreach (Label philosophy in philosophyList)
                {
                    philosophy.IsVisible = false;


                }

                sTimeLabel.TextColor = Color.White;

                foreach (Label sTime in sTimeList)
                {
                    sTime.IsVisible = false;
                }

            }
        }

        void PhilosopiesTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Philosophies");

            if (thirtyOneLabel.IsVisible)
            { //hides philosophy labels

                philosophyLabel.TextColor = Color.White;

                foreach (Label philosophy in philosophyList)
                {
                    philosophy.IsVisible = false;
                }

            }
            else
            { //reveals philosophy labels

                philosophyLabel.TextColor = Color.Tan;

                foreach (Label philosophy in philosophyList)
                {
                    philosophy.IsVisible = true;
                }

            }
        }

        void StimesTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("S Times");

            if (anywayLabel.IsVisible)
            { //hides philosophy labels

                sTimeLabel.TextColor = Color.White;

                foreach (Label sTime in sTimeList)
                {
                    sTime.IsVisible = false;
                }

            }
            else
            { //reveals philosophy labels

                sTimeLabel.TextColor = Color.Tan;

                foreach (Label sTime in sTimeList)
                {
                    sTime.IsVisible = true;
                }

            }

        }

        void MepsRunsTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("MEPS Runs");


        }

        void BedohaveMenuTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Bedohave");

            if(!Application.Current.Properties.ContainsKey("bedohaveType")){
                BedohaveTypeSelectorTapped(null, null);
            } else {

                Debug.WriteLine(Application.Current.Properties["bedohaveType"]);

                if (Application.Current.Properties["bedohaveType"].Equals("Open Entry"))
                {

                    NavigationPage navPage = new NavigationPage(new BedohaveEntryPage())
                    {
                        BackgroundColor = Color.FromHex("#726c5d"),
                        BarBackgroundColor = Color.FromHex("#0d4816"),
                        BarTextColor = Color.White
                    };


                    mdp.Detail = navPage;

                    mdp.Master.Title = "Bedohave";

                    mdp.IsPresented = false;

                } else {

                    TabbedPage tabbedPage = new TabbedPage()
                    {
                        BackgroundColor = Color.FromHex("#726c5d"),
                        BarBackgroundColor = Color.FromHex("#0d4816"),
                        BarTextColor = Color.White
                    };

                    tabbedPage.Children.Add(new BedohaveListPage("BE",null){
                        Title = "BE"
                    });

                    tabbedPage.Children.Add(new BedohaveListPage("DO",null)
                    {
                        Title = "DO"
                    });

                    tabbedPage.Children.Add(new BedohaveListPage("HAVE",null)
                    {
                        Title = "HAVE"
                    });
                    

                    NavigationPage navPage = new NavigationPage(tabbedPage){
                        BackgroundColor = Color.FromHex("#726c5d"),
                        BarBackgroundColor = Color.FromHex("#0d4816"),
                        BarTextColor = Color.White
                    };


                    mdp.Detail = navPage;

                    mdp.Master.Title = "Bedohave";

                    mdp.IsPresented = false;

                }



            }

            //TabbedPage tabbedPage = new TabbedPage()
            //{
            //    BackgroundColor = Color.FromHex("#726c5d"),
            //    BarBackgroundColor = Color.FromHex("#0d4816"),
            //    BarTextColor = Color.White
            //};

            //tabbedPage.Children.Add(new BedohaveEntryPage());
            //tabbedPage.Children.Add(new BedohaveListPage());

            //NavigationPage navPage = new NavigationPage(tabbedPage){
            //    BackgroundColor = Color.FromHex("#726c5d"),
            //    BarBackgroundColor = Color.FromHex("#0d4816"),
            //    BarTextColor = Color.White
            //};


            //mdp.Detail = navPage;

            //mdp.Master.Title = "Bedohave";

            //mdp.IsPresented = false;
        }

        async void BedohaveTypeSelectorTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Bedohave Type Selector");

            var typeSelection = await DisplayActionSheet("Would you like your Bedohave to be in a blank entry, or in a bulleted list?",
                                                         "cancel",
                                                         null,
                                                         "Open Entry",
                                                         "Bulleted List");

            switch(typeSelection){
                case "cancel":

                    break;

                case "Open Entry":

                    Debug.WriteLine("Open Entry");
                    if(!Application.Current.Properties.ContainsKey("bedohaveType")){

                        Application.Current.Properties.Add("bedohaveType", "Open Entry");

                    } else {

                        Application.Current.Properties["bedohaveType"] = "Open Entry";

                    }
                    await Application.Current.SavePropertiesAsync();
                    break;

                case "Bulleted List":

                    Debug.WriteLine("List");
                    if (!Application.Current.Properties.ContainsKey("bedohaveType"))
                    {

                        Application.Current.Properties.Add("bedohaveType", "List");

                    }
                    else
                    {

                        Application.Current.Properties["bedohaveType"] = "List";

                    }
                    await Application.Current.SavePropertiesAsync();
                    break;

            }

            await Application.Current.SavePropertiesAsync();


            if (Application.Current.Properties["bedohaveType"].Equals("Open Entry"))
            {

                NavigationPage navPage = new NavigationPage(new BedohaveEntryPage())
                {
                    BackgroundColor = Color.FromHex("#726c5d"),
                    BarBackgroundColor = Color.FromHex("#0d4816"),
                    BarTextColor = Color.White
                };


                mdp.Detail = navPage;

                mdp.Master.Title = "Bedohave";

                mdp.IsPresented = false;

            }
            else
            {

                TabbedPage tabbedPage = new TabbedPage()
                {
                    BackgroundColor = Color.FromHex("#726c5d"),
                    BarBackgroundColor = Color.FromHex("#0d4816"),
                    BarTextColor = Color.White
                };

                tabbedPage.Children.Add(new BedohaveListPage("BE", null)
                {
                    Title = "BE"
                });

                tabbedPage.Children.Add(new BedohaveListPage("DO", null)
                {
                    Title = "DO"
                });

                tabbedPage.Children.Add(new BedohaveListPage("HAVE", null)
                {
                    Title = "HAVE"
                });


                NavigationPage navPage = new NavigationPage(tabbedPage)
                {
                    BackgroundColor = Color.FromHex("#726c5d"),
                    BarBackgroundColor = Color.FromHex("#0d4816"),
                    BarTextColor = Color.White
                };


                mdp.Detail = navPage;

                mdp.Master.Title = "Bedohave";

                mdp.IsPresented = false;

            }
        }

        void MindGamesTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Mind Games");

            NavigationPage navPage = new NavigationPage(new MindGamesPage())
            {
                BackgroundColor = Color.FromHex("#726c5d"),
                BarBackgroundColor = Color.FromHex("#0d4816"),
                BarTextColor = Color.White
            };

            mdp.Detail = navPage;

            mdp.Master.Title = "Mind Games";

            mdp.IsPresented = false;


        }

        void SMEACgenTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("SMEAC Generator");

            NavigationPage navPage = new NavigationPage(new SMEACgeneratorPage())
            {
                BackgroundColor = Color.FromHex("#726c5d"),
                BarBackgroundColor = Color.FromHex("#0d4816"),
                BarTextColor = Color.White
            };

            mdp.Detail = navPage;

            mdp.Master.Title = "Smeac Generator";


            mdp.IsPresented = false;


        }

        void thirtyOneTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("31 Flavors");

            mdp.IsPresented = false;


            //NavigationPage pageWrapper = createNavWrapper("People are like ogres... they have layers. People are also like ice cream, but ice cream does not have layers.......", "31 Flavors");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.thirtyOne, "31 Flavors", false);

            mdp.Master.Title = "31 Flavors";

            mdp.Detail = pageWrapper;

        }

        void icebergTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Iceberg");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("Its like wait wait wait... like a candy corn... but the white part is the little stuff... and the rest is the good...", "Iceberg Theory");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.iceberg, "Iceberg Theory", false);

            mdp.Master.Title = "Iceberg Theory";

            mdp.Detail = pageWrapper;


        }

        void tlsTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("TLS");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("You gotta show people how to eat potatoes. You gotta learn how the good potatoes taste. You gotta not frown... like ever.", "TLS");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.tls, "TLS", false);


            mdp.Master.Title = "TLS";

            mdp.Detail = pageWrapper;

        }

        void ebaTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("EBA");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("When you say you don't like someone's shoes, they are sad. When you say you like their shoes, they are happy. When you tell them you have better shoes, they are sad, but will then buy better shoes later on that day.", "Emotional Bank Account");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.eba, "Emotional Bank Account", false);

            mdp.Master.Title = "Emotional Bank Account";

            mdp.Detail = pageWrapper;

        }

        void wizardTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Wizard");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("This guy tells a dog that if he wants to bark really loud, he should bark really loud because he is a dog and can already bark really loud since he's a dog and barks loudly already due to the fact that he is a dog and can bark really loud from his face... which is a dog face keep in mind.", "The Wizard");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.wizard, "The Wizard", false);

            mdp.Master.Title = "The Wizard";

            mdp.Detail = pageWrapper;

        }

        void potatoHeadTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Potato Head");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("It's a face, but it also has a butt flap and sometimes when you are bored or don't like your arms you can put things in your but flap but you can also borrow things from other peoples faces too.", "Mr. Potato Head");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.potatoHead, "Mr. Potato Head", false);

            mdp.Master.Title = "Mr. Potato Head";

            mdp.Detail = pageWrapper;

        }

        void microwaveTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Microwave Mentality");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("Life is like a pizza roll.", "Microwave Mentality");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.microwave, "Microwave Mentality", false);

            mdp.Master.Title = "Microwave Mentality";

            mdp.Detail = pageWrapper;

        }

        void fiveTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("5,4,3,2,1");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("5:People are stupid and lame 4: Puzzles are really hard 3: Pizza rolls good 2: Cave Bagels 1: Fatcamp","5,4,3,2,1");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.fiveFour, "5,4,3,2,1", false);

            mdp.Master.Title = "5,4,3,2,1 Most Important Words";

            mdp.Detail = pageWrapper;

        }

        void bedohaveTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Bedohave");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("You be things and do things so that you can have things, just don't try to have things first","Chief Bedohave");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.bedohave, "Bedohave", false);

            mdp.Master.Title = "Chief Bedohave";

            mdp.Detail = pageWrapper;

        }

        void darTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("D+A=R");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("Life math.","D + A = R");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.dPlusA, "D + A = R", false);

            mdp.Master.Title = "D + A = R";

            mdp.Detail = pageWrapper;

        }

        void smeacTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("SMEAC");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("Some Marsupials Eventually Ate Chowder.","Smeac");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.smeac, "SMEAC", false);

            mdp.Master.Title = "SMEAC";

            mdp.Detail = pageWrapper;

        }

        void mepsTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("MEPS");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("Hitler had a great plan didn't he? He really planned out his goals, took steps towards them, and really shared himself with others.","MEPS");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.meps, "MEPS", false);

            mdp.Master.Title = "MEPS";

            mdp.Detail = pageWrapper;

        }

        void traitsTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Leadership Traits");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("I don't know who this JJ guy is, but he sure DID know how to TIE a BUCKLE.","Leadership Traits");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.traits, "Leadership Traits", false);

            mdp.Master.Title = "Leadership Traits";

            mdp.Detail = pageWrapper;

        }

        void principlesTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Leadership Principles");
            mdp.IsPresented = false;

            //NavigationPage pageWrapper = createNavWrapper("Remember at least five of them for the test.","Leadership Principles");

            NavigationPage pageWrapper = createNavWrapper(PhilosophyResources.principles, "Leadership Principles", false);

            mdp.Master.Title = "Leadership Principles";

            mdp.Detail = pageWrapper;

        }

        void aCreedTapped(object sender, System.EventArgs e)
        {
            String title = "A Creed To Live By";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.aCreed, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void aWishTapped(object sender, System.EventArgs e)
        {
            String title = "A Wish For Leaders";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.aWish, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void anywayTapped(object sender, System.EventArgs e)
        {
            String title = "Anyway";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.anyway, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void attitudeTapped(object sender, System.EventArgs e)
        {
            String title = "Attitude";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.attitude, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void birdByBirdTapped(object sender, System.EventArgs e)
        {
            String title = "Bird By Bird";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.birdByBird, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void coversationsWithGodTapped(object sender, System.EventArgs e)
        {
            String title = "Conversations With God";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.conversationsWithGod, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void desiderataTapped(object sender, System.EventArgs e)
        {
            String title = "Desiderata";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.desiderata, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void dontWeAllTapped(object sender, System.EventArgs e)
        {
            String title = "Don't We All";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.dontWeAll, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void faithTrustLoveTapped(object sender, System.EventArgs e)
        {
            String title = "Faith, Trust, and Love Equals Devotion";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.faithTrustLove, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void footprintsTapped(object sender, System.EventArgs e)
        {
            String title = "Footprints";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.footprints, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void godsPromiseTapped(object sender, System.EventArgs e)
        {
            String title = "God’s Promise";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.godsPromise, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void howToMakeTapped(object sender, System.EventArgs e)
        {
            String title = "How To Make a Beautiful Life";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.howToMake, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void ifPoemTapped(object sender, System.EventArgs e)
        {
            String title = "If";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.ifPoem, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void ifEverTapped(object sender, System.EventArgs e)
        {
            String title = "If Ever There Was A Time";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.ifEverThereWas, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void ifIWereAnyBetterTapped(object sender, System.EventArgs e)
        {
            String title = "If I Were Any Better, I Would Be Twins";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.ifIWereBetter, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void justToTapped(object sender, System.EventArgs e)
        {
            String title = "Just To Keep You Going";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.justToKeep, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void lincolnsFailuresTapped(object sender, System.EventArgs e)
        {
            String title = "Lincoln's Failures";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.lincolnsFailures, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void littleEyesBoyTapped(object sender, System.EventArgs e)
        {
            String title = "Little Eyes Are Watching You (Boy)";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.littleEyesBoy, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void littleEyesGirlTapped(object sender, System.EventArgs e)
        {
            String title = "Little Eyes Are Watching You (Girl)";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.littleEyesGirl, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void liveYourDashTapped(object sender, System.EventArgs e)
        {
            String title = "Live Your Dash";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.liveYourDash, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void mayonnaiseJarTapped(object sender, System.EventArgs e)
        {
            String title = "Mayonnaise Jar and Two Cups of Coffee";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.mayonnaiseJar, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void myPledgeTapped(object sender, System.EventArgs e)
        {
            String title = "My Pledge";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.myPledge, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void paidInFullTapped(object sender, System.EventArgs e)
        {
            String title = "Paid In Full";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.paidInFull, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void quotesTapped(object sender, System.EventArgs e)
        {
            String title = "Quotes";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.quotes, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void rememberTapped(object sender, System.EventArgs e)
        {
            String title = "Remember";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.remember, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void spreadingYourLightTapped(object sender, System.EventArgs e)
        {
            String title = "Spreading Your Light";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.spreadingYourLight, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void strivingTapped(object sender, System.EventArgs e)
        {
            String title = "Striving";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.striving, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void successEmersonTapped(object sender, System.EventArgs e)
        {
            String title = "Success (Emerson)";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.successEmerson, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void successUnknownTapped(object sender, System.EventArgs e)
        {
            String title = "Success (Unknown)";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.successUnknown, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void bridgeBuilderTapped(object sender, System.EventArgs e)
        {
            String title = "The Bridge Builder";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.bridgeBuilder, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void carrotEggCoffeeBeanTapped(object sender, System.EventArgs e)
        {
            String title = "The Carrot, The Egg, and The Coffee Bean";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.carrotEggCoffeeBean, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void emperorsSeedTapped(object sender, System.EventArgs e)
        {
            String title = "The Emperor's Seed";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.emperorsSeed, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void generalsPoemTapped(object sender, System.EventArgs e)
        {
            String title = "The General's Poem (The Winner's Creed)";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.generalsPoem, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void manInTheArenaTapped(object sender, System.EventArgs e)
        {
            String title = "The Man In The Arena";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.manInTheArena, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void manInTheGlassTapped(object sender, System.EventArgs e)
        {
            String title = "The Man In The Glass";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.manInTheGlass, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void optimistCreedTapped(object sender, System.EventArgs e)
        {
            String title = "The Optimist Creed";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.theOptimistCreed, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void paradoxTapped(object sender, System.EventArgs e)
        {
            String title = "The Paradox of Our Age";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.paradoxOfOurAge, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void prayerOfaSportsmanTapped(object sender, System.EventArgs e)
        {
            String title = "The Prayer Of A Sportsman";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.prayerOfaSportsman, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void roadNotTakenTapped(object sender, System.EventArgs e)
        {
            String title = "The Road Not Taken";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.roadNotTaken, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void sheepdogsPoemTapped(object sender, System.EventArgs e)
        {
            String title = "The Sheepdog's Poem";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.sheepdogsPoem, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void starfishTapped(object sender, System.EventArgs e)
        {
            String title = "The Starfish Story";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.starfishStory, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void thisIsTheBeginningTapped(object sender, System.EventArgs e)
        {
            String title = "This Is The Beginning of a New Day";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.thisIsTheBeginning, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void toRiskTapped(object sender, System.EventArgs e)
        {
            String title = "To Risk";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.toRisk, title, false);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void universalPrayerTapped(object sender, System.EventArgs e)
        {
            String title = "Universal Prayer";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.universalPrayer, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void vigilTapped(object sender, System.EventArgs e)
        {
            String title = "Vigil";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.vigil, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        void weShallBeFreeTapped(object sender, System.EventArgs e)
        {
            String title = "We Shall Be Free";

            Debug.WriteLine(title);

            mdp.IsPresented = false;

            NavigationPage pageWrapper = createNavWrapper(STimeResources.weShallBeFree, title, true);

            mdp.Master.Title = title;

            mdp.Detail = pageWrapper;

        }

        public NavigationPage createNavWrapper(String bodyText, String titleIn, bool isCenetered)
        {

            PhilosophiesPage newPage = new PhilosophiesPage();
            newPage.setBodyText(bodyText, isCenetered);

            if (Device.RuntimePlatform != Device.iOS)
            {
                newPage.Title = titleIn;
            }

            NavigationPage pageWrapper = new NavigationPage(newPage);
            pageWrapper.BarTextColor = Color.White;
            pageWrapper.BarBackgroundColor = Color.FromHex("#0d4816");

            return pageWrapper;

        }
    }
}
